import os

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from bottle import request, response, route, run, static_file, template


UNKNOWN_IMAGE_URL = (
    "https://lastfm-img2.akamaized.net/i/u/300x300/c6f59c1e5e7240a4c0d427abd71f3dbb"
)


def _lastfm_response_to_song(response):
    song = {"is_playing": False}
    try:
        track = response["recenttracks"]["track"][0]
        if "nowplaying" not in track.get("@attr", ""):
            return

        song["artist"] = track["artist"]["#text"]
        song["album"] = track["album"]["#text"] or None
        song["title"] = track["name"]
        song["image_url"] = track["image"][-1]["#text"] or UNKNOWN_IMAGE_URL

    except (KeyError, IndexError):
        return

    return song


@route("/")
def index():
    session = requests.Session()
    retry_policy = Retry(
        total=3, read=3, connect=3, backoff_factor=0.5, status_forcelist=(500, 502, 504)
    )
    adapter = HTTPAdapter(max_retries=retry_policy)
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    response = session.get(
        "http://ws.audioscrobbler.com/2.0/",
        params={
            "method": "user.getrecenttracks",
            "api_key": os.getenv("LASTFM_API_KEY"),
            "format": "json",
            "user": request.query.get("user"),
        },
    )
    response.raise_for_status()

    data = _lastfm_response_to_song(response.json()) or {}
    data["playing"] = bool(data)
    data["refresh_rate"] = os.getenv("REFRESH_RATE", 5)

    response.content_type = "text/html; charset=UTF-8"
    return template(open("templates/index.tpl").read(), **data)


@route("/sample")
def sample():
    data = {
        "playing": True,
        "title": "Morgenlektüre zur Konfitüre - Original Version",
        "artist": "Flitz&Suppe",
        "image_url": "https://lastfm-img2.akamaized.net/i/u/300x300/f9a7ebbdc7a8ee179bfc9739c3f77038.jpg",
    }

    if request.query.get("empty"):
        data = {"playing": False}

    response.content_type = "text/html; charset=UTF-8"
    return template(open("templates/sample.tpl").read(), **data)


@route("/static/<filename>")
def server_static(filename):
    return static_file(filename, root="static")


if __name__ == "__main__":
    run(host="0.0.0.0", port=8080)

