<!DOCTYPE html>
<html>
  <head>
    <title>Last.fm Thing</title>
  </head>
  <body>
    <style>
    body {
      height: 100vh;
      width: 100vw;
      background-color: #DDD;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .wrapper {
      overflow: hidden;
      border-radius: 3px;
      box-shadow: 0 0 15px 0 rgba(0,0,0,0.5);
      width: 50%;
      height: 50%;
    }
    </style>
    <div class="wrapper">
      % if playing:
        % include('templates/widget.tpl')
      % else:
        % include('templates/empty.tpl')
      % end
    </div>
  </body>
</html>
