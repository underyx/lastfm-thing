<!DOCTYPE html>
<html>
  <head>
    <title>Last.fm Thing</title>
    <meta http-equiv="refresh" content="{{ refresh_rate }}">
  </head>
  <style>
    body,html {
      margin: 0;
      padding: 0;
    }
    * {
        box-sizing: border-box;
    }
    body {
      height: 100vh;
      width: 100vw;
    }
  </style>
  <body>
    % if playing:
      % include('templates/widget.tpl')
    % else:
      % include('templates/empty.tpl')
    % end
  </body>
</html>
