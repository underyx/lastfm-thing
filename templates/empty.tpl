<style>
  .empty-state {
    background-color: white;
    position: relative;
    height: 100%;
    padding: 0 20px;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    color: rgba(0, 0, 0, 0.7);
    font-family: sans-serif;
    text-align: left;
    line-height: 1.25;
    font-size: 26px;
  }

  .icon {
    height: 60px;
    width: 60px;
    opacity: 0.35;
  }

  .message {
    margin-left: 35px;
  }

  .url {
    color: rgba(0, 0, 255, 0.7);
    font-size: 20px;
  }
</style>

<div class="empty-state">
  <img src="static/note.svg" class="icon" />
  <div class="message">
    <b>Go play something!</b>
    <br />
    <span class="url">musicbox.local</span>
  </div>
</div>
