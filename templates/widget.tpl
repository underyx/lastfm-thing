<style>
  .widget {
    position: relative;
    height: 100%;
    padding: 0 20px;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    overflow: hidden;
  }

  .album-art {
    box-sizing: content-box;
    position: absolute;
    height: 100%;
    width: 100%;
    z-index: 10;
    background-image: url('{{ image_url }}');
    background-position: center center;
    background-size: cover;
    filter: saturate(2) blur(10px) brightness(0.5);
    margin-left: -60px;
    padding: 40px;
  }

  .equalizer {
    position: absolute;
    height: 100%;
    width: 100%;
    z-index: 15;
    background-image: url('/static/equalizer.svg');
    background-position: left 0 bottom -86px;
    background-repeat: repeat-x;
    margin-left: -20px;
  }

  .line {
    color: rgba(255, 255, 255, 0.8);
    font-family: sans-serif;
    text-align: center;
    line-height: 1.5;
    z-index: 20;
  }

  .line .artist,
  .line .title {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  .line .artist::before {
    content: 'by ';
  }

  .line .title {
    font-size: 26px;
  }
  .line .artist {
    font-size: 20px;
  }
</style>

<div class="widget">
  <div class="album-art"></div>
  <div class="equalizer"></div>
  <div class="line">
    <div class="title">{{ title }}</div>
    <div class="artist">{{ artist }}</div>
  </div>
</div>
